DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS Sessions;

CREATE TABLE Users(
    user_id INTEGER PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    score INTEGER
);

CREATE TABLE Sessions (
    session_id INTEGER PRIMARY KEY,
    user_id INT NOT NULL,
    start_date DATE NOT NULL,
    stop_date DATE,
    FOREIGN KEY(user_id) REFERENCES Users(user_id) 
);

INSERT INTO Users(user_id, name, score) VALUES (11, "User1", 1);
INSERT INTO Users(user_id, name, score) VALUES (22, "User2", 2);
INSERT INTO Users(user_id, name, score) VALUES (33, "User3", 3);
INSERT INTO Users(user_id, name, score) VALUES (99, "User99", 99);

INSERT INTO Sessions(user_id, start_date, stop_date) VALUES (11, datetime(), NULL);
INSERT INTO Sessions(user_id, start_date, stop_date) VALUES (22, datetime(), NULL);
INSERT INTO Sessions(user_id, start_date, stop_date) VALUES (33, datetime(), NULL);
INSERT INTO Sessions(user_id, start_date, stop_date) VALUES (99, datetime(), datetime());

import requests
import json

url = "http://localhost:1101"

def test_hello():
    response = requests.get(f"{url}") 
    print("Test hello", response, response.text)

def get_all_users():
    response = requests.get(f"{url}/users")
    print("Test get all users", response, response.text)

def test_create_user():
    data = {
        'user_name': 'pyhton user'
    }
    headers = {
        'Content-Type': 'application/json'
    }
    response = requests.post(f"{url}/users/new", headers=headers, data=json.dumps(data)) 
    print("Test create user response", response, response.text)

def start_session():
    data = {
        'user_name': 'User99'
    }
    headers = {
        'Content-Type': 'application/json'
    }
    response = requests.post(f"{url}/sessions/new", headers=headers, data=json.dumps(data)) 
    print("Test start session response", response, response.text)

def end_session():
    data = {
        'user_name': 'User99'
    }
    headers = {
        'Content-Type': 'application/json'
    }
    response = requests.post(f"{url}/sessions/end", headers=headers, data=json.dumps(data)) 
    print("Test end session response", response, response.text)

if __name__ == "__main__":
    test_hello()
    get_all_users()
    test_create_user()
    start_session()
    end_session()

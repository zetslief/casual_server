use std::sync::Mutex;

use actix_web::{App, HttpServer, web::Data};
use rusqlite::Connection;

pub mod db;
pub mod routes;

pub struct AppState {
    pub connection: Mutex<Connection>
}

const PATH: &'static str = "./db/punks.db";

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .app_data(Data::new(AppState {
                connection: Mutex::new(Connection::open(&PATH).unwrap())
            }))
            .service(routes::hello)
            .service(routes::get_all_users)
            .service(routes::create_user)
            .service(routes::start_session)
            .service(routes::end_session)
    })
    .bind(("localhost", 1101))?
    .run()
    .await
}

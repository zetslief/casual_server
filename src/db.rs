use rusqlite::{Row, Connection, Result, Error, params};
use chrono::naive::NaiveDateTime;

#[derive(Debug, serde::Serialize)]
pub struct User {
    pub user_id: u32,
    pub name: String,
    pub score: u32
}

impl TryFrom<&Row<'_>> for User {
    type Error = Error;

    fn try_from(value: &Row) -> Result<Self, Self::Error> {
        Ok(User {
            user_id: value.get(0)?,
            name: value.get(1)?,
            score: value.get(2)?
        })
    }
}

fn row_to_user(row: &Row) -> Result<User> {
    User::try_from(row)
}

pub fn get_all_users(db: &Connection) -> Result<Vec<User>> {
    let mut stmt = db.prepare("SELECT user_id, name, score FROM Users")?;
    let users = stmt.query_map([], row_to_user)?;
    Ok(users.map(Result::unwrap).collect())
}

pub fn find_user_by_id(db: &Connection, user_id: u32) -> Result<Option<User>> {
    let mut user_stmt = db.prepare(
        "SELECT user_id, name, score FROM Users WHERE user_id == ?")?;
    let mut users = user_stmt.query_map(params!(user_id), row_to_user)?;
    if let Some(user) = users.next() {
        user.map(Option::from)
    } else {
        Ok(None)
    }
}

pub fn find_user_by_name(db: &Connection, name: &str) -> Result<Option<User>> {
    let mut user_stmt = db.prepare(
        "SELECT user_id, name, score FROM Users WHERE name == ?")?;
    let mut users = user_stmt.query_map(params!(name), row_to_user)?;
    if let Some(user) = users.next() {
        user.map(Option::from)
    } else {
        Ok(None)
    }
}

pub fn create_user(db: &Connection, name: &str, score: u32) -> Result<usize> {
    let mut user_stmt = db.prepare(
        "INSERT INTO Users(name, score) VALUES (?, ?)")?;
    user_stmt.execute(params!(name, score))
}

pub fn find_or_create_user(
    name: &str,
    connection: &rusqlite::Connection,
) -> Result<User, rusqlite::Error> {
    if let Some(user) = find_user_by_name(&connection, name)? {
        Ok(user)
    } else {
        create_user(&connection, name, 0)?;
        Ok(find_user_by_name(&connection, name)?.expect("User must exist!"))
    }
}

pub fn delete_user(db: &mut Connection, user_id: u32) -> Result<()> {
    let tx = db.transaction()?;
    {
        let mut session_stmt = tx.prepare(
            "DELETE from Sessions WHERE user_id = ?")?;
        let mut user_stmt = tx.prepare(
            "DELETE from Users WHERE user_id = ?")?;

        session_stmt.execute(params!(user_id))?;
        user_stmt.execute(params!(user_id))?;
    }
    tx.commit()
}


#[derive(Debug)]
pub struct Session {
    pub session_id: u32,
    pub user_id: u32,
    pub start_date: NaiveDateTime,
    pub stop_date: Option<NaiveDateTime>
}

impl TryFrom<&Row<'_>> for Session {
    type Error = Error;

    fn try_from(value: &Row<'_>) -> Result<Self, Self::Error> {
        let start = helpers::convert_datetime_from_database(value.get::<_, String>(2)?);
        let stop = value
            .get::<_, Option<String>>(3)?
            .map(helpers::convert_datetime_from_database);
        Ok(Session {
            session_id: value.get(0)?,
            user_id: value.get(1)?,
            start_date: start,
            stop_date: stop
        })
    }
}

fn row_to_session(row: &Row) -> Result<Session> {
    row.try_into()
}

pub fn get_all_sessions(db: &Connection) -> Result<Vec<Session>> {
    let mut sessions_request = db.prepare(
        "SELECT session_id, user_id, start_date, stop_date FROM Sessions")?;
    let sessions = sessions_request.query_map([], row_to_session)?;
    Ok(sessions.map(Result::unwrap).collect())
}

pub fn get_all_started_sessions(db: &Connection) -> Result<Vec<Session>> {
    let mut sessions_request = db.prepare(
        "SELECT session_id, user_id, start_date, stop_date FROM Sessions WHERE stop_date IS NULL")?;
    let started_sessions = sessions_request.query_map([], row_to_session)?;
    Ok(started_sessions.map(Result::unwrap).collect())
}

pub fn get_all_finished_sessions(db: &Connection) -> Result<Vec<Session>> {
    let mut sessions_request = db.prepare(
        "SELECT session_id, user_id, start_date, stop_date FROM Sessions WHERE stop_date IS NOT NULL")?;
    let finished_sessions = sessions_request.query_map([], row_to_session)?;
    Ok(finished_sessions.map(Result::unwrap).collect())
}

pub fn find_started_session_by_user_id(db: &Connection, user_id: u32) -> Result<Option<Session>> {
    let mut stmt = db.prepare(
        "SELECT session_id, user_id, start_date, stop_date FROM Sessions WHERE stop_date IS NULL and user_id = ?"
    )?;
    let mut sessions = stmt.query_map(params!(user_id), row_to_session)?;
    if let Some(session) = sessions.next() {
        session.map(Option::from)
    } else {
        Ok(None)
    }
}

pub fn start_session(db: &mut Connection, user_id: u32) -> Result<()> {
    let tx = db.transaction()?;
    {
        let mut stmt = tx.prepare(
            "INSERT INTO Sessions(user_id, start_date, stop_date) VALUES (?, ?, NULL)")?;
        stmt.execute(params!(user_id, helpers::datetime()))?;
    }
    tx.commit()
}

pub fn end_session(db: &mut Connection, user_id: u32) -> Result<()> {
    // TODO: will this transaction fail if there is no started session for this user?
    let tx = db.transaction()?;
    {
        let mut stmt = tx.prepare(
            "UPDATE Sessions SET stop_date = ? WHERE stop_date IS NULL AND user_id = ?"
        )?;
        stmt.execute(params!(helpers::datetime(), user_id))?;
    }
    tx.commit()
}

mod helpers {
    use super::*;

    pub fn convert_datetime_from_database(datetime: String) -> NaiveDateTime {
        NaiveDateTime::parse_from_str(&datetime, "%Y-%m-%d %H:%M:%S")
            .expect("Database has valid datetime")
}

    pub fn datetime() -> String {
        chrono::Utc::now().format("%Y-%m-%d %H:%M:%S").to_string()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn main() -> Result<()> {

        let path = "./db/punks.db";
        let mut db = Connection::open(&path)?;

        let users = get_all_users(&db)?;
        println!("All users:{users:?}");

        let sessions = get_all_sessions(&db)?;
        println!("All sessions:{sessions:?}");

        let started_sessions = get_all_started_sessions(&db)?;
        println!("Started sessions:{started_sessions:?}");

        let finished_sessions = get_all_finished_sessions(&db)?;
        println!("Finished sessions:{finished_sessions:?}");

        let user_99 = find_user_by_id(&db, 99)?;
        println!("User99: {user_99:?}");

        let user_66 = find_user_by_id(&db, 66)?;
        println!("User99: {user_66:?}");

        if let Some(user) = find_user_by_name(&db, "User77")? {
            delete_user(&mut db, user.user_id)?;
            let deleted_user = find_user_by_id(&db, user.user_id)?;
            println!("Deleted user: {deleted_user:?}");
        } else {
            let user_77 = create_user(&db, "User77".to_owned(), 77)?;
            println!("User77: {user_77:?}");
        }

        let started_sessions_user_99 = find_started_session_by_user_id(&db, 99)?;
        println!("Started session for user99: {started_sessions_user_99:?}");

        if let Some(session) = started_sessions_user_99 {
            println!("Ending session...");
            end_session(&mut db, session.user_id)
        } else {
            println!("Starting session...");
            start_session(&mut db, 99)
        }
    }
}

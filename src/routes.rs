use actix_web::{get, post, web, HttpResponse, Responder};

use crate::AppState;

use crate::db;

macro_rules! error_request {
    ($e:expr, $message:literal) => {
        $e.json(APIError { error : format!($message) })
    }
}

macro_rules! ok {
    ($expr:expr) => {
        HttpResponse::Ok().json($expr)
    }
}

macro_rules! bad_request {
    ($message:literal) => {
        error_request!(HttpResponse::BadRequest(), $message)
    }
}

macro_rules! internal_error {
    ($message:literal) => {
        error_request!(HttpResponse::InternalServerError(), $message)
    }
}

macro_rules! forbidden {
    ($message:literal) => {
        error_request!(HttpResponse::Forbidden(), $message)
    }
}

#[derive(serde::Serialize)]
pub struct APIError
{
    pub error: String
}

#[derive(serde::Deserialize, Debug)]
pub struct User {
    pub id: u32
}

#[derive(serde::Deserialize, Debug)]
pub struct CreateUser {
    pub user_name: String
}

#[get("/")]
pub async fn hello() -> impl Responder {
    ok!("Hello, Punk!")
}

#[post("/users/new")]
pub async fn create_user(
    body: web::Json<CreateUser>,
    data: web::Data<AppState>
) -> impl Responder {
    let user = body.into_inner();
    match db::find_or_create_user(&user.user_name, &data.connection.lock().unwrap()) {
        Ok(user) => ok!(user),
        Err(error) => internal_error!("Failed to create user: {error}"),
    }
}

#[post("/sessions/start")]
pub async fn start_session(
    body: web::Json<User>,
    data: web::Data<AppState>
) -> impl Responder {
    let user = body.into_inner();
    let mut conn = data.connection.lock().unwrap();
    match db::find_user_by_id(&conn, user.id) {
        Ok(Some(_)) => (),
        Ok(None) => return bad_request!("User not found"),
        Err(error) => return internal_error!("Error: {error}")
    };
    match db::find_started_session_by_user_id(&conn, user.id) {
        Ok(Some(session)) => forbidden!("Session already started: {session:?}"),
        Ok(None) => 
            match db::start_session(&mut conn, user.id) {
                Ok(_) => ok!("Session started!"),
                Err(error) => internal_error!("Failed to start session: {error}")
            },
        Err(error) => internal_error!("Error: {error}"),
    }
}

#[post("/sessions/end")]
pub async fn end_session(
    body: web::Json<User>,
    data: web::Data<AppState>
) -> impl Responder {
    println!("Session finished!");
    let user = body.into_inner();
    let mut conn = data.connection.lock().unwrap();
    println!("Session finished! {}", user.id);
    match db::find_user_by_id(&conn, user.id) {
        Ok(Some(_)) => (),
        Ok(None) => return bad_request!("User not found"),
        Err(error) => return internal_error!("Error: {error}")
    };
    match db::find_started_session_by_user_id(&conn, user.id) {
        Ok(Some(_)) => match db::end_session(&mut conn, user.id) {
            Ok(_) => {
                println!("Session finished! {}", user.id);
                ok!("Session finished!")
            },
            Err(error) => {
                println!("Failed to finish session! {}", user.id);
                internal_error!("Failed to finish session: {error}")
            },
        },
        Ok(None) => {
            println!("Failed to finish session! {}", user.id);
            forbidden!("No started session found")
        },
        Err(error) => {
            println!("Failed to finish session! {} {}", user.id, error);
            internal_error!("Error: {error}")
        },
    }
}

#[get("/users")]
pub async fn get_all_users(data: web::Data<AppState>) -> impl Responder {
    let conn = &data.connection.lock().unwrap();
    let mut users = match db::get_all_users(&conn) {
        Ok(users) => users,
        Err(error) => return internal_error!("Error: {error}"),
    };
    users.sort_by(|left, right| right.score.cmp(&left.score));
    ok!(users)
}
